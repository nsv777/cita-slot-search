# Cita slot search
### Purpose
When you're an extranjero en Espana, you need to obtain la tarjeta del residente (aka TIE).  But to get a 'cita' (reservation) to actually get it, may be tricky because of the high demand. The script making it easier, by automating cita availability checking process.
### Howto
0. Copy config example
```shell
cp cita_config.ini.example cita_config.ini
```
1. Update config according to your needs. If you don't want use you real N.I.E. for checking, you can generate random one here: https://generadordni.es/#home
2. Install requirements
```shell
pip install -r requirements.txt
```
3. Run 
```shell
python do_cita.py 
```

