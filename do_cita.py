from configparser import ConfigParser

from classes.cita import Cita

if __name__ == '__main__':
    config = ConfigParser()
    config.read('cita_config.ini')

    cita = Cita(
        province=config['cita']['province'],
        procedure=config['cita']['procedure'],
        nie=config['cita']['nie'],
        name=config['cita']['name'],
        nationality=config['cita']['nationality']
    )
    cita.cita_search()
