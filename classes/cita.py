import time

from selenium.webdriver import ActionChains, Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select


class Cita(object):
    def cita_search(self):
        self.browser.get(self.cita_url)
        self.browser.implicitly_wait(time_to_wait=10)

        self.browser.implicitly_wait(time_to_wait=60)

        self.step_0_access_the_procedure()
        self.step_0_accept_cookies()
        self.step_1_select_province()
        self.step_2_select_procedure()
        self.step_3_accept_terms()
        self.step_4_nie_name_nationality()
        self.step_5_identity_confirm()

        time.sleep(20)

    def step_0_access_the_procedure(self):
        self._xpath_click(xpath='//*[@id="submit"]')

    def step_0_accept_cookies(self):
        self._click(element_id='cookie_action_close_header')

    def step_1_select_province(self):
        province_select = Select(self.browser.find_element(by=By.XPATH, value='//*[@id="form"]'))
        province_select.select_by_visible_text(text=self.province)

        self._click(element_id='btnAceptar')

    def step_2_select_procedure(self):
        procedure_select = Select(self.browser.find_element(by=By.XPATH, value='//*[@id="tramiteGrupo[1]"]'))
        procedure_select.select_by_visible_text(self.procedure)
        self._click(element_id='btnAceptar')

    def step_3_accept_terms(self):
        self._click(element_id='btnEntrar')

    def step_4_nie_name_nationality(self):
        self._type(element_id='txtIdCitado', text=self.nie)
        self._type(element_id='txtDesCitado', text=self.name)
        self._select(element_id='txtPaisNac', visible_text=self.nationality)

        self._click(element_id='btnEnviar')

    def step_5_identity_confirm(self):
        self._click(element_id='btnEnviar')

    def _select(self, element_id, visible_text):
        select = Select(self.browser.find_element(by=By.ID, value=element_id))
        select.select_by_visible_text(visible_text)

    def _type(self, element_id, text, ttw=1):
        self.browser.find_element(by=By.ID, value=element_id).clear()
        self.browser.find_element(by=By.ID, value=element_id).send_keys(text)
        self.browser.implicitly_wait(time_to_wait=ttw)

    def _click(self, element_id, ttw=1):
        self.browser.find_element(By.ID, element_id).click()
        self.browser.implicitly_wait(time_to_wait=ttw)

    def _class_click(self, element_name, ttw=1):
        self.browser.find_element(by=By.CLASS_NAME, value=element_name).click()
        self.browser.implicitly_wait(time_to_wait=ttw)

    def _css_click(self, css_selector, ttw=1):
        self.browser.find_element(by=By.CSS_SELECTOR, value=css_selector).click()
        self.browser.implicitly_wait(time_to_wait=ttw)

    def _xpath_click(self, xpath, ttw=1):
        self.browser.find_element(by=By.XPATH, value=xpath).click()
        self.browser.implicitly_wait(time_to_wait=ttw)

    def _hover_click(self, element_name):
        element = self.browser.find_element(by=By.NAME, value=element_name)
        ActionChains(self.browser).move_to_element(element).perform()
        self._class_click("hover")

    def close_browser(self):
        self.browser.close()

    def __init__(
            self,
            province,
            procedure,
            nie,
            name,
            nationality,
            cita_url='https://sede.administracionespublicas.gob.es/pagina/index/directorio/icpplus',
    ):
        self.browser = Chrome()
        self.cita_url = cita_url
        self.province = province
        self.procedure = procedure
        self.nie = nie
        self.name = name
        self.nationality = nationality
